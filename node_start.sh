#!/bin/bash

set -e

echo 'Installing deps'
npm install

echo 'Start'
npm run start:dev
