FROM node:14.18.1-alpine

LABEL maintainer="enzogeron@gmail.com"

WORKDIR /var/www/pickit

COPY package.json ./

RUN npm install

COPY .eslintrc.js nest-cli.json tsconfig.json tsconfig.build.json typeorm.config.ts ./

COPY .env /var/www/pickit/.env

CMD ["npm", "run", "start:dev", "--preserveWatchOutput"]
