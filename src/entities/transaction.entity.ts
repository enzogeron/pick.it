import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'
import { TransactionInterface } from '../interfaces/transaction.interface'
import { Car } from './car.entity'
import { Owner } from './owner.entity'

@Entity()
export class Transaction implements TransactionInterface {
  @PrimaryGeneratedColumn('uuid') id: string

  @Column({ type: 'simple-array' })
  services: string[]

  @ManyToOne(() => Car, (car) => car.transactions)
  car: Car

  @ManyToOne(() => Owner, (owner) => owner.transactions)
  owner: Owner

  @CreateDateColumn() created_at: Date
  @UpdateDateColumn() updated_at: Date
}
