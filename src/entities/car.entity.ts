import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm'
import { CarInterface } from '../interfaces/car.interface'
import { Owner } from './owner.entity'
import { Transaction } from './transaction.entity'
import { OneToMany } from 'typeorm'

@Entity()
export class Car implements CarInterface {
  @PrimaryGeneratedColumn('uuid') id: string

  @Column({ type: 'varchar', length: '255', nullable: false })
  mark: string

  @Column({ type: 'varchar', length: '255', nullable: false })
  model: string

  @Column({ type: 'varchar', length: '255', nullable: false })
  year: string

  @Column({ type: 'varchar', length: '255', nullable: false })
  patent: string

  @Column({ type: 'varchar', length: '255', nullable: false })
  colour: string

  @ManyToOne((type) => Owner) @JoinColumn({ name: 'owner_id' }) owner: Owner

  @OneToMany((type) => Transaction, (transaction) => transaction.car)
  transactions: Transaction[]

  @CreateDateColumn() created_at: Date
  @UpdateDateColumn() updated_at: Date
}
