import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'
import { OwnerInterface } from '../interfaces/owner.interface'
import { Car } from './car.entity'
import { Transaction } from './transaction.entity'

@Entity()
export class Owner implements OwnerInterface {
  @PrimaryGeneratedColumn('uuid') id: string

  @Column({ type: 'varchar', length: '255', nullable: false })
  name: string

  @Column({ type: 'varchar', length: '255', nullable: false })
  lastname: string

  @OneToMany(() => Car, (Car) => Car.owner)
  cars: Car[]

  @OneToMany(() => Transaction, (transaction) => transaction.owner)
  transactions: Transaction[]

  @CreateDateColumn() created_at: Date
  @UpdateDateColumn() updated_at: Date
}
