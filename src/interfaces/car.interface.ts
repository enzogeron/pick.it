export interface CarInterface {
  mark: string
  model: string
  year: string
  patent: string
  colour: string
}
