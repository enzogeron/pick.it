export enum ServiceTypes {
  OIL_CHANGE = 'OIL_CHANGE',
  FILTER_CHANGE = 'FILTER_CHANGE',
  BELT_CHANGE = 'BELT_CHANGE',
  GENERAL_REVIEW = 'GENERAL_REVIEW',
  PAINT = 'PAINT',
  OTHER = 'OTHER',
}

export interface TransactionInterface {
  services: string[]
}
