import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigModule } from '@nestjs/config'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { typeOrmConfigAsync } from '../typeorm.config'
import { CarsModule } from './modules/cars/cars.module'
import { OwnersModule } from './modules/owners/owners.module'
import { TransactionModule } from './modules/transactions/transactions.module'

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync(typeOrmConfigAsync),
    CarsModule,
    OwnersModule,
    TransactionModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
