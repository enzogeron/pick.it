import { Injectable, ServiceUnavailableException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { Owner } from '../../entities/owner.entity'
import { CreateOwnerDto } from './dtos/owner.dto'

@Injectable()
export class OwnersService {
  constructor(
    @InjectRepository(Owner)
    private readonly ownerRepository: Repository<Owner>,
  ) {}

  async create(req: CreateOwnerDto) {
    try {
      return this.ownerRepository.save(req)
    } catch (error) {
      console.error('ownersService - create', error)
      return new ServiceUnavailableException(`Failed create`)
    }
  }

  async getById(idOwner: string) {
    if (!idOwner) return
    return this.ownerRepository.findOne({ where: { id: idOwner } })
  }
}
