import { IsArray, IsNotEmpty, IsString } from 'class-validator'
import { Car } from '../../../entities/car.entity'
export class CreateOwnerDto {
  @IsString()
  @IsNotEmpty()
  name: string

  @IsString()
  @IsNotEmpty()
  lastname: string
}
