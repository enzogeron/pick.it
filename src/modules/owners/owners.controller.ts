import { Body, Controller, Post } from '@nestjs/common'
import { OwnersService } from './owners.service'
import { CreateOwnerDto } from './dtos/owner.dto'

@Controller('owners')
export class OwnersController {
  constructor(private readonly ownersService: OwnersService) {}

  @Post('create')
  async create(@Body() req: CreateOwnerDto) {
    return this.ownersService.create(req)
  }
}
