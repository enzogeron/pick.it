import { IsNotEmpty, IsOptional, IsString } from 'class-validator'

export class CreateCarDto {
  @IsString()
  @IsNotEmpty()
  readonly mark: string

  @IsString()
  @IsNotEmpty()
  readonly model: string

  @IsString()
  @IsNotEmpty()
  readonly year: string

  @IsString()
  @IsNotEmpty()
  readonly patent: string

  @IsString()
  @IsNotEmpty()
  readonly colour: string

  @IsString()
  @IsOptional()
  readonly owner_id: string
}

export class UpdateCarDto {
  @IsString()
  @IsOptional()
  readonly mark?: string

  @IsString()
  @IsOptional()
  readonly model?: string

  @IsString()
  @IsOptional()
  readonly year?: string

  @IsString()
  @IsOptional()
  readonly patent?: string

  @IsString()
  @IsOptional()
  readonly colour?: string
}

export class DeleteCarDto {
  @IsString()
  @IsNotEmpty()
  readonly idCar: string
}
