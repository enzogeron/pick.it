import { Injectable, ServiceUnavailableException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { Car } from '../../entities/car.entity'
import { CreateCarDto, UpdateCarDto, DeleteCarDto } from './dtos/car.dto'
import { OwnersService } from '../owners/owners.service'

@Injectable()
export class CarsService {
  constructor(
    @InjectRepository(Car)
    private readonly carRepository: Repository<Car>,
    private readonly ownerService: OwnersService,
  ) {}

  async create(req: CreateCarDto) {
    try {
      const owner = await this.ownerService.getById(req.owner_id)
      return this.carRepository.save({ ...req, owner })
    } catch (error) {
      console.error('carsService - create', error)
      return new ServiceUnavailableException(`Failed create`)
    }
  }

  async delete(req: DeleteCarDto) {
    try {
      const deleteCar = await this.carRepository.delete(req.idCar)
      let message = 'Not deleted'
      if (deleteCar.affected) {
        message = 'Delete successful'
      }
      return {
        idCar: req.idCar,
        message,
      }
    } catch (error) {
      console.error('carsService - delete', error)
      return new ServiceUnavailableException(`Failed delete`)
    }
  }

  async update(idCar: string, req: UpdateCarDto) {
    try {
      const car = await this.carRepository.update(idCar, req)
      let message = 'Not update'
      if (car.affected) {
        message = 'Update successful'
      }
      return {
        idCar,
        message,
      }
    } catch (error) {
      console.error('carsService - update', error)
      return new ServiceUnavailableException(`Failed update`)
    }
  }

  async getAll() {
    try {
      return this.carRepository.find({
        relations: ['owner'],
      })
    } catch (error) {
      console.error('carsService - getAll', error)
      return new ServiceUnavailableException(`Failed getAll`)
    }
  }

  async getById(idCar: string) {
    if (!idCar) return
    return this.carRepository.findOne({
      where: { id: idCar },
      relations: ['owner'],
    })
  }

  async getAllServices(idCar: string) {
    try {
      const car = await this.carRepository.findOne({
        where: { id: idCar },
        relations: ['transactions'],
      })
      return car.transactions
    } catch (error) {
      console.error('carsService - getAllServices', error)
      return new ServiceUnavailableException(`Failed getAllServices`)
    }
  }
}
