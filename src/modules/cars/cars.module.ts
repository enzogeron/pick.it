import { Module } from '@nestjs/common'
import { CarsService } from './cars.service'
import { CarsController } from './cars.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Car } from '../../entities/car.entity'
import { OwnersModule } from '../owners/owners.module'

@Module({
  imports: [TypeOrmModule.forFeature([Car]), OwnersModule],
  exports: [CarsService],
  controllers: [CarsController],
  providers: [CarsService],
})
export class CarsModule {}
