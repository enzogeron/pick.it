import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common'
import { CarsService } from './cars.service'
import { CreateCarDto, UpdateCarDto, DeleteCarDto } from './dtos/car.dto'

@Controller('cars')
export class CarsController {
  constructor(private readonly carsService: CarsService) {}

  @Get()
  async getAll() {
    return this.carsService.getAll()
  }

  @Get(':idCar')
  async getById(@Param('idCar') idCar: string) {
    return this.carsService.getById(idCar)
  }

  @Post('create')
  async create(@Body() req: CreateCarDto) {
    return this.carsService.create(req)
  }

  @Patch('update/:idCar')
  async update(@Body() req: UpdateCarDto, @Param('idCar') idCar: string) {
    return this.carsService.update(idCar, req)
  }

  @Delete('delete')
  async delete(@Body() req: DeleteCarDto) {
    return this.carsService.delete(req)
  }

  @Get(':idCar/services')
  async getServices(@Param('idCar') idCar: string) {
    return this.carsService.getAllServices(idCar)
  }
}
