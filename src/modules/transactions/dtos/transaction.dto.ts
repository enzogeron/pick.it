import { IsArray, IsEnum, IsNotEmpty, IsString } from 'class-validator'
import { ServiceTypes } from 'src/interfaces/transaction.interface'

export class CreateTransactionDto {
  @IsNotEmpty()
  @IsEnum(ServiceTypes, { each: true })
  services: ServiceTypes[]

  @IsArray()
  @IsNotEmpty()
  prices: number[]

  @IsString()
  @IsNotEmpty()
  owner_id: string

  @IsString()
  @IsNotEmpty()
  car_id: string
}
