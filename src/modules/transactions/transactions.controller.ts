import { Body, Controller, Get, Post } from '@nestjs/common'
import { TransactionsService } from './transactions.service'
import { CreateTransactionDto } from './dtos/transaction.dto'

@Controller('transactions')
export class TransactionsController {
  constructor(private readonly transactionService: TransactionsService) {}

  @Get()
  async getALl() {
    return this.transactionService.getAll()
  }

  @Post('create')
  async create(@Body() req: CreateTransactionDto) {
    return this.transactionService.create(req)
  }
}
