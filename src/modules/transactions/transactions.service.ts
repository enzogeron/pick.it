import {
  BadRequestException,
  Injectable,
  ServiceUnavailableException,
} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { Transaction } from '../../entities/transaction.entity'
import { CreateTransactionDto } from './dtos/transaction.dto'
import { CarsService } from '../cars/cars.service'
import { OwnersService } from '../owners/owners.service'
import { ServiceTypes } from '../../interfaces/transaction.interface'
import { COLOUR_GRAY } from '../../utils/index'

@Injectable()
export class TransactionsService {
  constructor(
    @InjectRepository(Transaction)
    private readonly transactionRepository: Repository<Transaction>,
    private readonly carsService: CarsService,
    private readonly ownerService: OwnersService,
  ) {}

  async create(req: CreateTransactionDto) {
    const car = await this.carsService.getById(req.car_id)
    const owner = await this.ownerService.getById(req.owner_id)

    if (!car || !owner) {
      throw new BadRequestException(
        'It is necessary to assign an owner and a car in the transaction',
      )
    }

    const accumulator = req.prices.reduce(
      (accumulator, curr) => accumulator + curr,
    )

    if (car.colour.toUpperCase() === COLOUR_GRAY) {
      const servicePaint = req.services.find(
        (service) => service === ServiceTypes.PAINT,
      )
      if (servicePaint)
        throw new BadRequestException(
          `It is not allowed to provide the painting service for gray cars`,
        )
    }

    const transaction = await this.transactionRepository.save({
      ...req,
      car,
      owner,
    })
    return {
      transaction_id: transaction.id,
      car_id: car.id,
      owner_id: owner.id,
      total_services: accumulator,
    }
  }

  async getAll() {
    try {
      return this.transactionRepository.find({
        relations: ['car', 'owner'],
      })
    } catch (error) {
      console.error('transactionService - getAll', error)
      return new ServiceUnavailableException(`Failed getAll`)
    }
  }
}
